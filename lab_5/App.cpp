#include <iostream>
#include "App.h"

App* singleton;

void timer(int id){
    // This will get called every 16 milliseconds after
    // you call it once
    
    // If you want to manipulate the app in here
    // do it through the singleton pointer
    // std::cout << "Timer called" << std::endl;
   	// std::cout << "X value:" << singleton->cat->getX() << std::endl;
   //	singleton->a->getX();
	if(singleton->a1->getX() <= -6.0) {
		singleton->a1->setX(2.0);
	}
	if(singleton->a2->getX() <= -6.0) {
		singleton->a2->setX(2.0);
	}

    glutTimerFunc(15, timer, id);
     singleton->a1->setX(singleton->a1->getX()-0.01);
     singleton->a2->setX(singleton->a2->getX()-0.01);
}


App::App(int argc, char** argv, int width, int height, const char* title): GlutApp(argc, argv, width, height, title){
	singleton = this;
   a1 = new TexRect("/Users/darshanbulsara/Desktop/cse_165/lab_5/bg.png", -2.0, 1.0, 4.0, 2.0);

   a2 = new TexRect("/Users/darshanbulsara/Desktop/cse_165/lab_5/bg.png", 2.0, 1.0, 4.0, 2.0);

   cat = new AnimatedRect ("/Users/darshanbulsara/Desktop/cse_165/lab_5/cat.png",4 ,2 , 100, true, true, -1.0, -0.2, 1.5, 0.6);
   timer(5);
}

void App::draw() {
	bool flag = true;

	a1->draw(0.0);
	a2->draw(0.0);
	cat->draw(0.6);

}

void App::keyDown(unsigned char key, float x, float y){
    if (key == 27){
        exit(0);
    }
}

App::~App(){
    std::cout << "Exiting..." << std::endl;
}
