#include "stage.h"
#include<iostream>
#include <vector>
#include <iterator>

using namespace std;

stage::stage() {
    stageSet.push_back(new Rect(-0.8, -0.2, 0.4, -0.35, 1.0, 0.0, 0.0));
    stageSet.push_back(new Rect(-0.26, 0.5, 0.6, -0.3, 0.0, 1.0, 0.0));
    stageSet.push_back(new Rect(-0.26, 0.35, 0.6, 0.55, 0.0, 1.0, 0.0));
    stageSet.push_back(new Rect(0.5, -0.2, 0.3, -1.0, 0.0, 0.0, 1.0));
    stageSet.push_back(new Rect(-0.8, 0.8, 0.4, 0.5, 1.0, 0.5, 0.0));
    stageSet.push_back(new Rect(-0.95, -0.3 , 1.9, -1.2, 1.0, 1.0, 1.0));
    

    
    //stageBorder = new Rect(-0.95, -0.3 , 1.9, -1.2, 1.0, 1.0, 1.0);
    //stageColumn1 = new Rect(-0.8, -0.2, 0.4, -0.35, 1.0, 0.0, 0.0);
    //stageColumn2 = new Rect(-0.26, 0.5, 0.6, -0.3, 0.0, 1.0, 0.0);
    //stageColumn3 = new Rect(-0.26, 0.35, 0.6, 0.55, 0.0, 1.0, 0.0);
    //stageColumn4 = new Rect(0.5, -0.2, 0.3, -1.0, 0.0, 0.0, 1.0);
    //stageColumn5 = new Rect(-0.8, 0.8, 0.4, 0.5, 1.0, 0.5, 0.0);
}

void stage::draw() const {
   // cout << stageSet.size() << endl;
    
    for(int i = 0; i < stageSet.size(); i++) {
        stageSet[i] ->draw();
    }
//    stageColumn1->draw();
//    stageColumn2->draw();
//    stageColumn3->draw();
//    stageColumn4->draw();
//    stageColumn5->draw();
//    stageBorder->draw();
}

stage::~stage(){
    //delete stageSet;
//    delete stageBorder;
//    delete stageColumn1;
//    delete stageColumn2;
//    delete stageColumn3;
//    delete stageColumn4;
}

