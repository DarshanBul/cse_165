#ifndef stage_h
#define stage_h

#include "Rect.h"
#include "AppComponent.h"
#include <vector>
using namespace std;

class stage: public AppComponent, public Rect {
    Rect* stageBorder;
    Rect* stageColumn1;
    Rect* stageColumn2;
    Rect* stageColumn3;
    Rect* stageColumn4;
    Rect* stageColumn5;
    
    vector<Rect*> stageSet;
    
public:
    stage();
    void draw() const;
    ~stage();
};

#endif /* stage_h */
